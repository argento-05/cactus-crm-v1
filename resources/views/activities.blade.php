<x-app-layout>

    @section('title', 'Activities')

    <div class="flex justify-between items-center mb-8">
        <h2 class="text-xl">Activities</h2>
    </div>

    <livewire:activity.activity-table />

</x-app-layout>
