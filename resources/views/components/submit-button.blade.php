@props([
    'label' => 'create',
    'createLabel' => 'Create',
    'creatingLabel' => 'Creating..',
    'updateLabel' => 'Update',
    'updatingLabel' => 'Updating..',
])

<x-secondary-button class="ms-4" wire:click="$dispatch('closeModal')" type="button" wire:target="submit"
    wire:loading.attr="disabled">
    Cancel
</x-secondary-button>

<button type="submit"
    class="button inline-flex items-center px-4 py-2 text-sm font-semibold text-white border border-transparent rounded-md shadow-sm bg-primary-500 hover:bg-primary-600 focus:outline-none focus:ring-2 focus:ring-offset-2 focus:ring-indigo-500"
    wire:target="submit" wire:loading.attr="disabled">
    @if ($label == 'create')
        <x-icon name="save" class="w-4 mr-1" solid /> <span wire:target="submit"
            wire:loading.delay.remove>{{ $createLabel }}</span>
        <span wire:target="submit" wire:loading.delay>{{ $creatingLabel }}</span>
    @else
        <x-icon name="refresh" class="w-4 mr-1" solid /> <span wire:target="submit"
            wire:loading.delay.remove>{{ $updateLabel }}</span>
        <span wire:target="submit" wire:loading.delay>{{ $updatingLabel }}</span>
    @endif
</button>
