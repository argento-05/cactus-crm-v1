<div>
    
    <div class="px-8 py-3 bg-gray-100 border-b-2 rounded-t-xl">
        {{ $header }}
    </div>

    <div>
        <form wire:submit="submit">
            @csrf
            <div class="px-8 pb-1">
                <x-errors />

                {{ $content }}
            </div>

            @if(isset($footer))
                <div class="border-b border-gray-200 border-m4"></div>
                <div class="pt-5 pb-5 border-t-2 bottom-pos rounded-b-xl">
                    <div class="flex justify-end px-8">
                        {{ $footer }}
                    </div>
                </div>
            @endif
        </form>
    </div>

</div>
