<x-modal-common>
    <x-slot name="header">
        <h3 class="flex flex-row items-center text-xl font-semibold leading-6 text-gray-700">
            <span class="inline-flex items-center justify-center w-8 h-8 mr-4 bg-gray-200 rounded-full">
                <x-icon name="pencil-alt" class="w-5 h-5 text-gray-400" />
            </span>
            Edit Customer
        </h3>
    </x-slot>

    <x-slot name="content">
        <div class="space-y-8 divide-y divide-gray-200 sm:space-y-5">
            <div class="flex flex-col pt-4">
                <div class="w-full mb-8 mr-6">
                    <h2 class="text-lg font-semibold text-gray-600">Customer information</h2>
                    <p class="mt-2 text-sm text-gray-500">Please avoid typographical errors while editing customer.</p>
                </div>
                <div class="w-full">
                    <div class="space-y-6 sm:space-y-5">
                        <div class="grid grid-cols-1 gap-y-5 sm:grid-cols-2 sm:gap-5 sm:items-start">
                            <x-input wire:model="customer.id" label="ID *" placeholder="ID" autofocus disabled />
                            <x-input wire:model="customer.name" label="Customer name *" placeholder="Customer name" />

                        </div>
                    </div>
                </div>

                <div class="w-full">
                    <div class="space-y-6 sm:space-y-5">
                        <div class="grid grid-cols-1 gap-y-5 sm:grid-cols-2 sm:gap-5 sm:items-start">
                            <x-input wire:model="customer.email" label="Email *" placeholder="Email" />
                            <x-input wire:model="customer.phone" label="Phone *" placeholder="Customer name" />

                        </div>
                    </div>
                </div>
            </div>
        </div>
    </x-slot>

    <x-slot name="footer">
        <x-submit-button label="update" />
    </x-slot>
</x-modal-common>
