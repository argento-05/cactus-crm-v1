<x-app-layout>

    @section('title', 'Customers')

    <div class="flex justify-between items-center mb-8">
        <h2 class="text-xl">Customers</h2>
    </div>

    <livewire:customer.customer-table />

</x-app-layout>
