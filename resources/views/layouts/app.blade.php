<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">

<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <title>{{ config('app.name', 'Laravel') }}</title>

    <!-- Fonts -->
    <link rel="preconnect" href="https://fonts.bunny.net">
    <link href="https://fonts.bunny.net/css?family=figtree:400,500,600&display=swap" rel="stylesheet" />

    @livewireStyles
    @filamentStyles

    <!-- Scripts -->
    @vite(['resources/css/app.css', 'resources/js/app.js'])
</head>

<body class="font-sans antialiased">

    <div class="min-h-screen bg-gray-100">
        <livewire:layout.navigation />

        <!-- Page Heading -->
        @if (isset($header))
            <header class="bg-white shadow">
                <div class="max-w-7xl mx-auto py-6 px-4 sm:px-6 lg:px-8">
                    {{ $header }}
                </div>
            </header>
        @endif

        <!-- Page Content -->
        <main>
            <div class="py-8 mx-auto max-w-screen-2xl sm:px-6 lg:px-8">

                <x-validation-errors class="my-4" />

                <!-- Replace with your content -->
                <div class="px-4 pb-4 sm:px-0">
                    {{ $slot }}
                </div>
                <!-- /End replace -->
            </div>
        </main>
    </div>

    @filamentScripts
    @livewireScripts
    @wireUiScripts

    @livewire('wire-elements-modal')
    @livewire('livewire-ui-spotlight')
</body>

</html>
