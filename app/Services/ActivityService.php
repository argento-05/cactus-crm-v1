<?php

namespace App\Services;

use App\Models\Activity;
use App\Services\Service;

class ActivityService extends Service
{
    /**
     * Activity rules
     *
     * @return mixed
     */
    public function rules(): mixed
    {
        return [
            'customer_id' => 'required',
            'type' => 'required',
            'description' => 'required',
        ];
    }

    /**
     * Activity update rules
     *
     * @return mixed
     */
    public function updateRules($id): mixed
    {
        return [
            'customer_id' => 'required',
            'type' => 'required',
            'status' => 'required',
            'description' => 'required',
        ];
    }

    /**
     * Activities lists
     *
     * @param  array  $filters
     * @param  bool|int  $paginate
     * @param  bool  $asBuilder
     * @return \Illuminate\Database\Eloquent\Collection|\Illuminate\Database\Eloquent\Builder
     */
    public function list(array $filters = [], bool|int $paginate = false, bool $asBuilder = false)
    {
        //TODO: For now we only return the list of activities of the logged in user,
        // we need to implement roles in the future => "admins" can see all activities, "operators" can only see their activities

        $query = $this->getFilteredQuery(
            query: Activity::query()->where('user_id', auth()->user()->id),
            pipelines: [
                \App\Filters\General\OrderFilter::class,
                \App\Filters\General\LimitFilter::class,
            ],
            filters: $filters,
            relationships: ['user', 'customer']
        );

        if ($asBuilder) {
            return $query;
        }

        if ($paginate) {
            return $query->fastPaginate($paginate);
        }

        return $query->get();
    }

    /**
     * Get single item
     *
     * @param  array  $filters
     * @return \App\Models\Activity|null
     */
    public function single(array $filters): ?Activity
    {
        return $this->getFilteredQuery(
            query: Activity::query(),
            pipelines: [
                \App\Filters\General\NameFilter::class,
            ],
            filters: $filters,
        )->first();
    }

    /**
     * Activity create
     *
     * @param  array  $data
     * @return \App\Models\Activity
     */
    public function create(array $data): Activity
    {
        $this->validate($data);

        $data['user_id'] = auth()->user()->id;

        $activity = Activity::create($data);

        return $activity->refresh();
    }

    /**
     * Activity update
     *
     * @param  \App\Models\Activity  $activity
     * @param  array  $data
     * @return \App\Models\Activity
     */
    public function update(Activity $activity, array $data): Activity
    {
        $this->validate($data, $this->updateRules($activity->id));

        $activity->update($data);

        return $activity->refresh();
    }

    /**
     * Activity delete
     *
     * @param  \App\Models\Activity  $activity
     * @return bool
     */
    public function delete(Activity $activity): bool
    {
        return $activity->delete();
    }

}
