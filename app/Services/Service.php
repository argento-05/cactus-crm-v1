<?php

namespace App\Services;

use App\Exceptions\PermissionException;
use App\Filters\FilterData;
use App\Services\Contracts\ServiceContract;
use Illuminate\Contracts\Database\Query\Builder;
use Illuminate\Pipeline\Pipeline;
use Illuminate\Support\Facades\Validator;
use Illuminate\Validation\UnauthorizedException as Unauthorized;
use Illuminate\Validation\ValidationException;
use Spatie\Permission\Exceptions\UnauthorizedException;

abstract class Service implements ServiceContract
{

    /**
     * Get validation rules
     *
     * @return mixed
     */
    public function rules(): mixed
    {
        return [];
    }

    /**
     * Get validation messages
     *
     * @return array
     */
    public function messages(): array
    {
        return [];
    }

    /**
     * Get validation attributes
     *
     * @return array
     */
    public function attributes(): array
    {
        return [];
    }

    /**
     * Validate data
     *
     * @param  array  $data
     * @param  array  $rules
     * @return bool
     *
     * @throws \Illuminate\Validation\ValidationException
     */
    public function validate(array $data, mixed $rules = null): bool
    {
        if (is_null($rules)) {
            $rules = $this->rules();
        }

        $validator = Validator::make(
            data: $data,
            rules: $rules,
            messages: $this->messages(),
            // customAttributes:$this->attributes() L10 update
        );

        if ($validator->fails()) {
            throw ValidationException::withMessages($validator->errors()->toArray());
        }

        return true;
    }

    /**
     * Get filtered query
     *
     * @param  \Illuminate\Contracts\Database\Query\Builder  $query
     * @param  array  $pipelines
     * @param  array  $filters
     * @param  array  $relationships
     * @return \Illuminate\Contracts\Database\Query\Builder
     */
    public function getFilteredQuery(Builder $query, array $pipelines, array $filters, array $relationships = [])
    {
        $query = app(Pipeline::class)
            ->send(new FilterData($query, $filters))
            ->through($pipelines)
            ->thenReturn()
            ->getBuilder();

        if (count($relationships) > 0) {
            $query->with($relationships);
        }

        return $query;
    }
}
