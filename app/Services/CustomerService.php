<?php

namespace App\Services;

use App\Models\Customer;
use App\Services\Service;
use Illuminate\Validation\Rule;

class CustomerService extends Service
{
    /**
     * Customer rules
     *
     * @return mixed
     */
    public function rules(): mixed
    {
        return [
            'name' => 'required|string|max:255',
            'email' => 'required|email|max:255|unique:customers,email',
            'phone' => 'required|string|max:255',
            'address' => 'required|string|max:255',
        ];
    }

    /**
     * Customer update rules
     *
     * @return mixed
     */
    public function updateRules($id): mixed
    {
        return [
            'name' => 'required|string|max:255',
            'email' => ['required', 'string', 'email', Rule::unique('customers', 'email')->ignore($id)],
            'phone' => ['required', 'string', 'max:255', Rule::unique('customers', 'phone')->ignore($id)],
            'address' => 'required|string|max:255',
        ];
    }

    /**
     * Assign user rules
     *
     * @return mixed
     */
    public function assignUserRules(): mixed
    {
        return [
            'assigned_user_id' => 'required',
        ];
    }

    /**
     * Customers lists
     *
     * @param  array  $filters
     * @param  bool|int  $paginate
     * @param  bool  $asBuilder
     * @return \Illuminate\Database\Eloquent\Collection|\Illuminate\Database\Eloquent\Builder
     */
    public function list(array $filters = [], bool|int $paginate = false, bool $asBuilder = false)
    {
        $query = $this->getFilteredQuery(
            query: Customer::query(),
            pipelines: [
                \App\Filters\General\NameFilter::class,
                \App\Filters\General\OrderFilter::class,
                \App\Filters\General\LimitFilter::class,
            ],
            filters: $filters,
            relationships: ['assignedUser']
        );

        if ($asBuilder) {
            return $query;
        }

        if ($paginate) {
            return $query->fastPaginate($paginate);
        }

        return $query->get();
    }

    /**
     * Get single item
     *
     * @param  array  $filters
     * @return \App\Models\Customer|null
     */
    public function single(array $filters): ?Customer
    {
        return $this->getFilteredQuery(
            query: Customer::query(),
            pipelines: [
                \App\Filters\General\NameFilter::class,
            ],
            filters: $filters,
        )->first();
    }

    /**
     * Customer create
     *
     * @param  array  $data
     * @return \App\Models\Customer
     */
    public function create(array $data): Customer
    {
        $this->validate($data);

        $customer = Customer::create($data);

        return $customer->refresh();
    }

    /**
     * Customer update
     *
     * @param  \App\Models\Customer  $customer
     * @param  array  $data
     * @return \App\Models\Customer
     */
    public function update(Customer $customer, array $data): Customer
    {
        $this->validate($data, $this->updateRules($customer->id));

        $customer->update($data);

        return $customer->refresh();
    }

    /**
     * Customer delete
     *
     * @param  \App\Models\Customer  $customer
     * @return bool
     */
    public function delete(Customer $customer): bool
    {
        return $customer->delete();
    }

    /**
     * Assign user
     *
     * @param  \App\Models\Customer  $customer
     * @param  array  $data
     * @return \App\Models\Customer
     */
    public function assignUser(Customer $customer, array $data): Customer
    {
        $this->validate($data, $this->assignUserRules($customer->id));

        $customer->update($data);

        return $customer->refresh();
    }
}
