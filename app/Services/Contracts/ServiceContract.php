<?php

namespace App\Services\Contracts;

interface ServiceContract
{
    /**
     * Validate data
     *
     * @param  array  $data
     * @return bool
     *
     * @throws Exception
     */
    public function validate(array $data): bool;

    /**
     * Get validation messages
     *
     * @return array
     */
    public function messages(): array;

    /**
     * Get validation attributes
     *
     * @return array
     */
    public function attributes(): array;
}
