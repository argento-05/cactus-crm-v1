<?php

namespace App\Traits;

use Filament\Tables\Table;

trait TableTrait
{
    /**
     * Create a new Table instance.
     *
     * @param  \Filament\Tables\Table  $table
     * @return \Filament\Tables\Table
     */
    public function table(Table $table): Table
    {
        return $table
            ->query($this->getTableQuery())
            ->columns($this->getTableColumns())
            ->filters($this->getTableFilters())
            ->headerActions($this->getHeaderActions())
            ->actions($this->getTableActions())
            ->bulkActions($this->getTableBulkActions())
            ->defaultSort(
                $this->getDefaultTableSortColumn(),
                $this->getDefaultTableSortDirection()
            )
            ->paginated($this->getPaginationOptions())
            ->queryStringIdentifier($this->getTableQueryStringIdentifier());
    }

    /**
     * Get table pagination options.
     *
     * @return array
     */
    public function getPaginationOptions(): array
    {
        if (method_exists($this, 'getTableRecordsPerPageSelectOptions')) {
            $pagination = $this->getTableRecordsPerPageSelectOptions();
        } else {
            $pagination = $this->getPaginationPageOptions();
        }

        // Sort pagination options
        sort($pagination);

        // If not contains "all" option, add it
        if (! in_array('all', $pagination)) {
            $pagination[] = 100;
        } else {
            // If contains "all" option, remove it
            $pagination = array_diff($pagination, ['all']);
            $pagination[] = 100;
        }

        // Unique pagination options
        $pagination = array_unique($pagination);

        return $pagination;
    }
}
