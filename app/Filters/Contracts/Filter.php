<?php

namespace App\Filters\Contracts;

use App\Filters\FilterData;
use Closure;

interface Filter
{
    /**
     * Method handle
     *
     * @param  FilterData  $filterData
     * @param  Closure  $next
     * @return void
     */
    public function handle(FilterData $filterData, Closure $next);
}
