<?php

namespace App\Filters\General;

use App\Filters\Contracts\Filter;
use App\Filters\FilterData;
use Closure;

class DateBetweenFilter implements Filter
{
    /**
     * filter
     *
     * @var mixed
     */
    protected $filter;

    /**
     * column
     *
     * @var mixed
     */
    protected $column;

    /**
     * Method __construct
     *
     * @param $filter $filter
     * @param $column $column
     * @return void
     */
    public function __construct(string $filter, string $column)
    {
        $this->filter = $filter;
        $this->column = $column;
    }

    /**
     * Date between filter handler.
     *
     * @param  FilterData  $filterData
     * @param  Closure  $next
     * @return void
     */
    public function handle(FilterData $filterData, Closure $next)
    {
        if ($filterData->getArgument($this->filter) === null) {
            return $next($filterData);
        }

        $data = collect($filterData->getArgument($this->filter))->filter()->values();
        if ($data->count() === 0) {
            return $next($filterData);
        }

        if ($data->filter()->count() === 1) {
            $filterData->getBuilder()->whereDate($this->column, '<=', $data[0]);
        } else {
            $filterData->getBuilder()->where(function ($query) use ($data) {
                $query->whereDate($this->column, '>=', $data[0]);
                $query->whereDate($this->column, '<=', $data[1]);
            });
        }

        return $next($filterData);
    }
}