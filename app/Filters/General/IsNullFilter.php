<?php

namespace App\Filters\General;

use App\Filters\Contracts\Filter;
use App\Filters\FilterData;
use Closure;

class IsNullFilter implements Filter
{
    /**
     * @var string
     */
    protected $filter;

    /**
     * @var string
     */
    protected $column;

    /**
     * @var bool
     */
    protected $isNull;

    /**
     * Method __construct
     *
     * @param  string  $filter
     * @param  string  $column
     * @param  bool  $isNull
     * @return void
     */
    public function __construct($filter, $column, $isNull = true)
    {
        $this->filter = $filter;
        $this->column = $column;
        $this->isNull = $isNull;
    }

    /**
     * Dynamic filter handler.
     *
     * @param  FilterData  $filterData
     * @param  Closure  $next
     * @return void
     */
    public function handle(FilterData $filterData, Closure $next)
    {
        if ($filterData->getArgument($this->filter) === null) {
            return $next($filterData);
        }

        if ($this->isNull) {
            $filterData->getBuilder()->whereNull($this->column);
        } else {
            $filterData->getBuilder()->whereNotNull($this->column);
        }

        return $next($filterData);
    }
}
