<?php

namespace App\Filters\General;

use App\Filters\Contracts\Filter;
use App\Filters\FilterData;
use Closure;

class DynamicFilter implements Filter
{
    /**
     * filter
     *
     * @var mixed
     */
    protected $filter;

    /**
     * operator
     *
     * @var mixed
     */
    protected $operator;

    /**
     * column
     *
     * @var mixed
     */
    protected $column;

    /**
     * Method __construct
     *
     * @param $filter $filter
     * @param $operator $operator
     * @param $column $column
     * @return void
     */
    public function __construct($filter, $operator = '=', $column = null)
    {
        $this->filter = $filter;
        $this->operator = $operator;
        $this->column = $column ?: $filter;
    }

    /**
     * Dynamic filter handler.
     *
     * @param  FilterData  $filterData
     * @param  Closure  $next
     * @return void
     */
    public function handle(FilterData $filterData, Closure $next)
    {
        if ($filterData->getArgument($this->filter) === null && $filterData->getArgument('or|'.$this->filter) === null) {
            return $next($filterData);
        }

        if ($filterData->getArgument($this->filter) !== null) {
            $filterData->getBuilder()->where($this->column, $this->operator, $filterData->getArgument($this->filter));
        }

        if ($filterData->getArgument('or|'.$this->filter) !== null) {
            $filterData->getBuilder()->orWhere($this->column, $this->operator, $filterData->getArgument('or|'.$this->filter));
        }

        return $next($filterData);
    }
}
