<?php

namespace App\Filters\General;

use App\Filters\Contracts\Filter;
use App\Filters\FilterData;
use Closure;

class RegisteredUserFilter implements Filter
{
    /**
     * Method __construct
     *
     * @param $type $type
     * @return void
     */
    public function __construct(protected $registered)
    {
        $this->registered = $registered;
    }

    /**
     * ID filter handler.
     *
     * @param  FilterData  $filterData
     * @param  Closure  $next
     * @return void
     */
    public function handle(FilterData $filterData, Closure $next)
    {
        $filterData->getBuilder()->whereHas('customer', function ($q) {
            if ($this->registered) {
                $q->where(function ($q1) {
                    $q1->where('pincode', '!=', null)->orWhere(function ($q2) {
                        $q2->where('password', '!=', null)->where('use_password', true);
                    });
                });
            } else {
                // Non Jwallet customers
                $q->where(function ($q1) {
                    $q1->where('pincode', null)->Where(function ($q2) {
                        $q2->where('password', null)->orwhere('use_password', false);
                    });
                });
            }

        });

        return $next($filterData);
    }
}
