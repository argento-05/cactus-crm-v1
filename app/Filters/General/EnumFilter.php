<?php

namespace App\Filters\General;

use App\Filters\Contracts\Filter;
use App\Filters\FilterData;
use Closure;

class EnumFilter implements Filter
{
    /**
     * Enum class.
     *
     * @var \UnitEnum
     */
    protected $enum;

    /**
     * Filter argument.
     *
     * @var string
     */
    protected $filter;

    /**
     * Column to filter.
     *
     * @var string
     */
    protected $column;

    /**
     * Method __construct
     *
     * @param $filter $filter
     * @param $column $column
     * @return void
     */
    public function __construct($enum, string $filter, string $column)
    {
        $this->enum = $enum;
        $this->filter = $filter;
        $this->column = $column;
    }

    /**
     * Date between filter handler.
     *
     * @param  FilterData  $filterData
     * @param  Closure  $next
     * @return void
     */
    public function handle(FilterData $filterData, Closure $next)
    {
        if ($filterData->getArgument($this->filter) === null) {
            return $next($filterData);
        }

        $enumValue = $this->enum::searchValueByName($filterData->getArgument($this->filter));

        if ($enumValue === null) {
            return $next($filterData);
        }

        $filterData->getBuilder()->where($this->column, $enumValue);

        return $next($filterData);
    }
}
