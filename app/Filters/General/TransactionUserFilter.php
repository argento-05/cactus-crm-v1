<?php

namespace App\Filters\General;

use App\Filters\Contracts\Filter;
use App\Filters\FilterData;
use App\Models\User;
use Closure;
use Illuminate\Contracts\Auth\Authenticatable;

class TransactionUserFilter implements Filter
{
    /**
     * User ID.
     *
     * @var \Illuminate\Contracts\Auth\Authenticatable|\App\Models\User
     */
    protected Authenticatable|User $user;

    /**
     * Create a new instance.
     *
     * @param  string|null  $userId
     * @return void
     */
    public function __construct(?string $userId = null)
    {
        $this->user = $userId ? User::find($userId) : auth()->user();
    }

    /**
     * ID filter handler.
     *
     * @param  FilterData  $filterData
     * @param  Closure  $next
     * @return void
     */
    public function handle(FilterData $filterData, Closure $next)
    {
        if ($this->user->type == 'admin') {
            return $next($filterData);
        }

        $filterData->getBuilder()->where(function ($query) {
            $query->where('from_id', $this->user->id)
                ->orWhere('to_id', $this->user->id);
        });

        return $next($filterData);

    }
}
