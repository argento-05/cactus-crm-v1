<?php

namespace App\Filters\General;

use App\Enums\Transaction\External\EntityTypeEnum;
use App\Filters\Contracts\Filter;
use App\Filters\FilterData;
use Closure;

class EntityTypeFilter implements Filter
{
    /**
     * Name filter handler.
     *
     * @param  FilterData  $filterData
     * @param  Closure  $next
     * @return void
     */
    public function handle(FilterData $filterData, Closure $next)
    {
        if ($filterData->getArgument('for_deposit')) {
            $filterData->getBuilder()->where('account_number', '!=', null);
        }

        if (! $filterData->getArgument('type')) {
            return $next($filterData);
        }
        if ($filterData->getArgument('type') == 'BANK') {
            $filterId = EntityTypeEnum::BANK->value;
        } elseif ($filterData->getArgument('type') == 'WALLET') {
            $filterId = EntityTypeEnum::WALLET->value;
        } else {
            return $next($filterData);
        }

        $filterData->getBuilder()->where('type_id', 'LIKE', $filterId);

        return $next($filterData);
    }
}
