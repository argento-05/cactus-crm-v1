<?php

namespace App\Filters\General;

use App\Filters\Contracts\Filter;
use App\Filters\FilterData;
use Closure;

class FAQCategoryFilter implements Filter
{

    /**
     * ID filter handler.
     *
     * @param  FilterData  $filterData
     * @param  Closure  $next
     * @return void
     */
    public function handle(FilterData $filterData, Closure $next)
    {
      if (auth()->user()->type == 'customer') 
      {
        if($filterData->getArgument('faq_category_id') != null)
        {
          $filterData->getBuilder()->where('faq_category_id', $filterData->getArgument('faq_category_id'));
        }else{
            $filterData->getBuilder()->where('faq_category_id', FAQ_Home_Category_ID);
        }
        
       }

        return $next($filterData);
    }
}
