<?php

namespace App\Filters\General;

use App\Filters\Contracts\Filter;
use App\Filters\FilterData;
use Closure;

class CallableFilter implements Filter
{
    /**
     * Argument.
     *
     * @var mixed
     */
    protected $argument;

    /**
     * Method __construct
     *
     * @param  array  $columns
     * @return void
     */
    public function __construct($argument = 'callable')
    {
        $this->argument = $argument;
    }

    /**
     * Except filter handler.
     *
     * @param  FilterData  $filterData
     * @param  Closure  $next
     * @return void
     */
    public function handle(FilterData $filterData, Closure $next)
    {
        if (! $filterData->getArgument($this->argument)) {
            return $next($filterData);
        }

        $callable = $filterData->getArgument($this->argument);

        if (! is_callable($callable)) {
            return $next($filterData);
        }

        // Execute in the filterData builder the callable function
        $filterData->getBuilder()->tap($callable);

        return $next($filterData);
    }
}
