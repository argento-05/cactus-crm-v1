<?php

namespace App\Filters\General;

use App\Filters\Contracts\Filter;
use App\Filters\FilterData;
use Closure;

class PublicRoleFilter implements Filter
{
    /**
     * UUID filter handler.
     *
     * @param  FilterData  $filterData
     * @param  Closure  $next
     * @return void
     */
    public function handle(FilterData $filterData, Closure $next)
    {
        if (! $filterData->getArgument('public_role')) {
            return $next($filterData);
        }

        $filterData->getBuilder()->whereHas('roles', function ($query) {
            $query->where('public', true);
        });

        return $next($filterData);
    }
}
