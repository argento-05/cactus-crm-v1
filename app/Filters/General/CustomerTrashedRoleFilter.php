<?php

namespace App\Filters\General;

use App\Filters\Contracts\Filter;
use App\Filters\FilterData;
use Closure;

class CustomerTrashedRoleFilter implements Filter
{


    /**
     * Method __construct
     *
     * @param $type $type
     * @return void
     */
    public function __construct(protected $type)
    {
        $this->type = $type;
    }

    /**
     * ID filter handler.
     *
     * @param  FilterData  $filterData
     * @param  Closure  $next
     * @return void
     */
    public function handle(FilterData $filterData, Closure $next)
    {
        if($filterData->getArgument('role_id') != null)
        {
            $filterData->getBuilder()->whereHas('customerTrashed', function($q) use ($filterData) { 
                $q->whereHas('customerRole', function($q) use ($filterData) { 
                    $q->where('customer_role_id',$filterData->getArgument('role_id'));
                });
            });
        }
        return $next($filterData);
    }
}
