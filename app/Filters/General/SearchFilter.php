<?php

namespace App\Filters\General;

use App\Filters\Contracts\Filter;
use App\Filters\FilterData;
use Closure;

class SearchFilter implements Filter
{
    /**
     * filter
     *
     * @var mixed
     */
    protected $columns;

    /**
     * Method __construct
     *
     * @param  array  $columns
     * @return void
     */
    public function __construct($columns = [])
    {
        $this->columns = $columns;
    }

    /**
     * Search filter handler.
     *
     * @param  FilterData  $filterData
     * @param  Closure  $next
     * @return void
     */
    public function handle(FilterData $filterData, Closure $next)
    {
        if ($filterData->getArgument('search') === null) {
            return $next($filterData);
        }

        if (count($this->columns) === 0) {
            return $next($filterData);
        }

        $filterData->getBuilder()->where(function ($query) use ($filterData) {
            foreach ($this->columns as $column) {
                // Search by relationship
                $column = explode('.', $column);
                $this->deepRelation($query, $column, $filterData);
            }
        });

        return $next($filterData);
    }

    private function deepRelation($query, $columns, $filterData, $deep = false) 
    {
        $column = array_shift($columns); 
        if ($deep) {
            if (empty($columns)) {
                $query->where($column, 'like', '%'.$filterData->getArgument('search').'%');
            } else {
                $query->whereHas($column, function ($query) use ($columns, $filterData) {
                    return $this->deepRelation($query, $columns, $filterData, true);
                });
            }           
        } else {
            if (empty($columns)) {
                $query->orWhere($column, 'like', '%'.$filterData->getArgument('search').'%');
            } else {
                $query->orWhereHas($column, function ($query) use ($columns, $filterData) {
                    return $this->deepRelation($query, $columns, $filterData, true);
                });
            }
        }

        return $query;
    }
}
