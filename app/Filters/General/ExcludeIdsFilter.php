<?php

namespace App\Filters\General;

use App\Filters\Contracts\Filter;
use App\Filters\FilterData;
use Closure;

class ExcludeIdsFilter implements Filter
{
    /**
     * Filter column.
     *
     * @var string
     */
    protected $filter;

    /**
     * Method __construct
     *
     * @param  string  $column
     * @return void
     */
    public function __construct($filter = 'id')
    {
        $this->filter = $filter;
    }

    /**
     * ID filter handler.
     *
     * @param  FilterData  $filterData
     * @param  Closure  $next
     * @return void
     */
    public function handle(FilterData $filterData, Closure $next)
    {
        $excludeIds = config('app.exclude_ids');

        if (! $excludeIds) {
            return $next($filterData);
        }

        $filterData->getBuilder()->whereNotIn($this->filter, explode(',', $excludeIds));

        return $next($filterData);
    }
}
