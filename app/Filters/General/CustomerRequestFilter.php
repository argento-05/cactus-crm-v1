<?php

namespace App\Filters\General;

use App\Filters\Contracts\Filter;
use App\Filters\FilterData;
use Closure;

class CustomerRequestFilter implements Filter
{
    /**
     * ID filter handler.
     *
     * @param  FilterData  $filterData
     * @param  Closure  $next
     * @return void
     */
    public function handle(FilterData $filterData, Closure $next)
    {
        if (auth()->check() && auth()->user()->type == 'customer') {
            $filterData->getBuilder()->where('id', auth()->user()->id);
        }

        return $next($filterData);
    }
}
