<?php

namespace App\Filters\General;

use App\Filters\Contracts\Filter;
use App\Filters\FilterData;
use Closure;

class ActiveFilter implements Filter
{

    /**
     * ID filter handler.
     *
     * @param  FilterData  $filterData
     * @param  Closure  $next
     * @return void
     */
    public function handle(FilterData $filterData, Closure $next)
    {
      if (auth()->user()->type == 'customer') {
        $filterData->getBuilder()->active();
        }

        return $next($filterData);
    }
}
