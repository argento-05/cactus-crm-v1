<?php

namespace App\Filters\General;

use App\Filters\Contracts\Filter;
use App\Filters\FilterData;
use Closure;

class DateBetweenFilter implements Filter
{
    /**
     * filter
     *
     * @var mixed
     */
    protected $filter;

    /**
     * column
     *
     * @var mixed
     */
    protected $column;

    /**
     * Method __construct
     *
     * @param $filter $filter
     * @param $column $column
     * @return void
     */
    public function __construct(string $filter, string $column)
    {
        $this->filter = $filter;
        $this->column = $column;
    }

    /**
     * Date between filter handler.
     *
     * @param  FilterData  $filterData
     * @param  Closure  $next
     * @return void
     */
    public function handle(FilterData $filterData, Closure $next)
    {
        if ($filterData->getArgument($this->filter) === null) {
            return $next($filterData);
        }

        $filterData->getBuilder()->whereBetween($this->column, $filterData->getArgument($this->filter));

        return $next($filterData);
    }
}
