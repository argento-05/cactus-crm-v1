<?php

namespace App\Filters\General;

use App\Filters\Contracts\Filter;
use App\Filters\FilterData;
use Closure;

class OrderFilter implements Filter
{
    /**
     * Dynamic filter handler.
     *
     * @param  FilterData  $filterData
     * @param  Closure  $next
     * @return void
     */
    public function handle(FilterData $filterData, Closure $next)
    {
        if ($filterData->getArgument('order') === null) {
            //Default order => created_at, desc
            $filterData->getBuilder()->orderBy('created_at', 'desc');
            return $next($filterData);
        }

        $direction = $filterData->getArgument('direction', 'asc');

        $filterData->getBuilder()->orderBy($filterData->getArgument('order'), $direction);

        return $next($filterData);
    }
}
