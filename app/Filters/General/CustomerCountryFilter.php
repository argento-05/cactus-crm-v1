<?php

namespace App\Filters\General;

use App\Filters\Contracts\Filter;
use App\Filters\FilterData;
use Closure;

class CustomerCountryFilter implements Filter
{


    /**
     * Method __construct
     *
     * @param $type $type
     * @return void
     */
    public function __construct(protected $type)
    {
        $this->type = $type;
    }

    /**
     * ID filter handler.
     *
     * @param  FilterData  $filterData
     * @param  Closure  $next
     * @return void
     */
    public function handle(FilterData $filterData, Closure $next)
    {
        if($filterData->getArgument('country_id') != null)
        {
            $filterData->getBuilder()->whereHas('customer', function($q) use ($filterData) { 
                $q->whereHas('person', function($q) use ($filterData) { 
                    $q->whereHas('firstAddress', function($q) use ($filterData) { 
                      $q->where('country_id',$filterData->getArgument('country_id'));
                });
            });
            });
        }
        return $next($filterData);
    }
}
