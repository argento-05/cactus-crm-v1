<?php

namespace App\Filters\General;

use App\Filters\Contracts\Filter;
use App\Filters\FilterData;
use Closure;

class WhereInFilter implements Filter
{
    /**
     * filter
     *
     * @var mixed
     */
    protected $filter;

    /**
     * column
     *
     * @var mixed
     */
    protected $columns;

    /**
     * Method __construct
     *
     * @param  string  $filter
     * @param  string|array  $column
     * @return void
     */
    public function __construct($filter, $columns)
    {
        $this->filter = $filter;
        $this->columns = $columns;
    }

    /**
     * Where in filter handler.
     *
     * @param  FilterData  $filterData
     * @param  Closure  $next
     * @return void
     */
    public function handle(FilterData $filterData, Closure $next)
    {
        if ($filterData->getArgument($this->filter) === null) {
            return $next($filterData);
        }

        if (is_string($this->columns)) {
            $filterData->getBuilder()->whereIn($this->columns, $filterData->getArgument($this->filter));

            return $next($filterData);
        }

        $filterData->getBuilder()->where(function ($query) use ($filterData) {
            foreach ($this->columns as $column) {
                $query->orWhereIn($column, $filterData->getArgument($this->filter));
            }
        });

        return $next($filterData);
    }
}
