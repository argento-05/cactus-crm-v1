<?php

namespace App\Filters\General;

use App\Filters\Contracts\Filter;
use App\Filters\FilterData;
use Closure;

class NameFilter implements Filter
{
    /**
     * Name filter handler.
     *
     * @param  FilterData  $filterData
     * @param  Closure  $next
     * @return void
     */
    public function handle(FilterData $filterData, Closure $next)
    {
        if (! $filterData->getArgument('name')) {
            return $next($filterData);
        }

        $filterData->getBuilder()->where('name', 'LIKE', $filterData->getArgument('name'));

        return $next($filterData);
    }
}
