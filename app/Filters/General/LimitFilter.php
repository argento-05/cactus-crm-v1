<?php

namespace App\Filters\General;

use App\Filters\Contracts\Filter;
use App\Filters\FilterData;
use Closure;

class LimitFilter implements Filter
{
    /**
     * Limit filter handler.
     *
     * @param  FilterData  $filterData
     * @param  Closure  $next
     * @return void
     */
    public function handle(FilterData $filterData, Closure $next)
    {
        if (! $filterData->getArgument('limit')) {
            return $next($filterData);
        }

        $filterData->getBuilder()->limit($filterData->getArgument('limit'));

        return $next($filterData);
    }
}
