<?php

namespace App\Filters\General;

use App\Filters\Contracts\Filter;
use App\Filters\FilterData;
use Closure;

class UuidFilter implements Filter
{
    /**
     * UUID filter handler.
     *
     * @param  FilterData  $filterData
     * @param  Closure  $next
     * @return void
     */
    public function handle(FilterData $filterData, Closure $next)
    {
        if (! $filterData->getArgument('id')) {
            return $next($filterData);
        }

        $filterData->getBuilder()->where('id', (string) $filterData->getArgument('id'));

        return $next($filterData);
    }
}
