<?php

namespace App\Filters\General;

use App\Filters\Contracts\Filter;
use App\Filters\FilterData;
use Closure;

class ExceptFilter implements Filter
{
    /**
     * Filter column.
     *
     * @var string
     */
    protected $column;

    /**
     * Method __construct
     *
     * @param  string  $column
     * @return void
     */
    public function __construct($column = 'id')
    {
        $this->column = $column;
    }

    /**
     * Except filter handler.
     *
     * @param  FilterData  $filterData
     * @param  Closure  $next
     * @return void
     */
    public function handle(FilterData $filterData, Closure $next)
    {
        if (! $filterData->getArgument('except')) {
            return $next($filterData);
        }

        $filterData->getBuilder()->where($this->column, '!=', $filterData->getArgument('except'));

        return $next($filterData);
    }
}
