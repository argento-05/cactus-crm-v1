<?php

namespace App\Filters\General;

use App\Filters\Contracts\Filter;
use App\Filters\FilterData;
use Closure;

class DynamicRelationFilter implements Filter
{
    /**
     * relation
     *
     * @var mixed
     */
    protected $relation;

    /**
     * filter
     *
     * @var mixed
     */
    protected $filter;

    /**
     * operator
     *
     * @var mixed
     */
    protected $operator;

    /**
     * column
     *
     * @var mixed
     */
    protected $column;

    /**
     * Method __construct
     *
     * @param $relation $relation
     * @param $filter $filter
     * @param $operator $operator
     * @param $column $column
     * @return void
     */
    public function __construct($relation, $filter, $operator = '=', $column = null)
    {
        $this->relation = $relation;
        $this->filter = $filter;
        $this->operator = $operator;
        $this->column = $column ?: $filter;
    }

    /**
     * Dynamic Relationship filter handler.
     *
     * @param  FilterData  $filterData
     * @param  Closure  $next
     * @return void
     */
    public function handle(FilterData $filterData, Closure $next)
    {
        if ($filterData->getArgument($this->filter) === null) {
            return $next($filterData);
        }

        $filterData->getBuilder()->whereHas($this->relation, function ($query) use ($filterData) {
            if ($this->operator === 'in') {
                $query->whereIn($this->column, $this->asArray($filterData->getArgument($this->filter)));
            } else {
                $query->where($this->column, $this->operator, $filterData->getArgument($this->filter));
            }
        });

        return $next($filterData);
    }

    /**
     * FIx a single value as array
     *
     * @param  mixed  $value
     * @return array
     */
    private function asArray($value): array
    {
        if (is_array($value)) {
            return $value;
        }

        return [$value];
    }
}
