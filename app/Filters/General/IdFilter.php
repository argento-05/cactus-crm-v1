<?php

namespace App\Filters\General;

use App\Filters\Contracts\Filter;
use App\Filters\FilterData;
use Closure;

class IdFilter implements Filter
{
    /**
     * ID filter handler.
     *
     * @param  FilterData  $filterData
     * @param  Closure  $next
     * @return void
     */
    public function handle(FilterData $filterData, Closure $next)
    {
        if (! $filterData->getArgument('id')) {
            return $next($filterData);
        }

        $filterData->getBuilder()->where('id', $filterData->getArgument('id'));

        return $next($filterData);
    }
}
