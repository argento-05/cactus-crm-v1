<?php

namespace App\Filters\General;

use App\Filters\Contracts\Filter;
use App\Filters\FilterData;
use Closure;

class RequestChannelFilter implements Filter
{


    /**
     * Method __construct
     *
     * @param $type $type
     * @return void
     */
    public function __construct(protected $type)
    {
        $this->type = $type;
    }

    /**
     * ID filter handler.
     *
     * @param  FilterData  $filterData
     * @param  Closure  $next
     * @return void
     */
    public function handle(FilterData $filterData, Closure $next)
    {

        $filterData->getBuilder()->where('request_channel', $this->type);

        return $next($filterData);
    }
}
