<?php

namespace App\Filters\General;

use App\Filters\Contracts\Filter;
use App\Filters\FilterData;
use Closure;

class ParentIdFilter implements Filter
{
    /**
     * Name filter handler.
     *
     * @param  FilterData  $filterData
     * @param  Closure  $next
     * @return void
     */
    public function handle(FilterData $filterData, Closure $next)
    {
        if (! $filterData->getArgument('parent_id')) {
            // return $next($filterData);
        }

        $filterData->getBuilder()->where('parent_id', $filterData->getArgument('parent_id') ?? null);

        return $next($filterData);
    }
}
