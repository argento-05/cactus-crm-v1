<?php

namespace App\Filters\General;

use App\Filters\Contracts\Filter;
use App\Filters\FilterData;
use Closure;

class RelationHasPivotValueFilter implements Filter
{
    /**
     * Filter column
     *
     * @var string
     */
    protected string $filter;

    /**
     * Relationship
     *
     * @var string
     */
    protected string $relationship;

    /**
     * Pivot column
     *
     * @var string
     */
    protected string $pivotColumn;

    /**
     * Method __construct
     *
     * @param  string  $filter
     * @param  string  $relationship
     * @param  string  $pivotColumn
     * @return void
     */
    public function __construct(string $filter, string $relationship, ?string $pivotColumn = null)
    {
        $this->filter = $filter;
        $this->relationship = $relationship;
        $this->pivotColumn = $pivotColumn ?: $filter;
    }

    /**
     * Dynamic filter handler.
     *
     * @param  FilterData  $filterData
     * @param  Closure  $next
     * @return void
     */
    public function handle(FilterData $filterData, Closure $next)
    {
        if ($filterData->getArgument($this->filter) === null) {
            return $next($filterData);
        }

        $filterData->getBuilder()
            ->whereHas($this->relationship, function ($query) use ($filterData) {
                $query->where($this->pivotColumn, $filterData->getArgument($this->filter));
            });

        return $next($filterData);
    }
}
