<?php

namespace App\Filters\General;

use App\Filters\Contracts\Filter;
use App\Filters\FilterData;
use Closure;

class AllowedCountriesFilter implements Filter
{
    /**
     * ID filter handler.
     *
     * @param  FilterData  $filterData
     * @param  Closure  $next
     * @return void
     */
    public function handle(FilterData $filterData, Closure $next)
    {
        if (! $filterData->getArgument('allowed_countries') || $filterData->getArgument('allowed_countries') !== true) {
            return $next($filterData);
        }

        $filterData->getBuilder()->allowedCountries();

        return $next($filterData);
    }
}
