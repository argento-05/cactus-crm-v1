<?php

namespace App\Filters\General;

use App\Filters\Contracts\Filter;
use App\Filters\FilterData;
use Closure;

class NotOwnedBy implements Filter
{


    /**
     * Method __construct
     *
     * @param $type $type
     * @return void
     */
    public function __construct(protected $field)
    {
        $this->field = $field;
    }

    /**
     * ID filter handler.
     *
     * @param  FilterData  $filterData
     * @param  Closure  $next
     * @return void
     */
    public function handle(FilterData $filterData, Closure $next)
    {

        $filterData->getBuilder()->where($this->field, '!=', auth()->user()->id);

        return $next($filterData);
    }
}
