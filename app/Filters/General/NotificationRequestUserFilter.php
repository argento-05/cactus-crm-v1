<?php

namespace App\Filters\General;

use App\Filters\Contracts\Filter;
use App\Filters\FilterData;
use Closure;

class NotificationRequestUserFilter implements Filter
{


    /**
     * ID filter handler.
     *
     * @param  FilterData  $filterData
     * @param  Closure  $next
     * @return void
     */
    public function handle(FilterData $filterData, Closure $next)
    {
        $filterData->getBuilder()->where('user_id', auth()->user()->id);

        return $next($filterData);
    }
}
