<?php

namespace App\Filters;

use Illuminate\Database\Eloquent\Builder;

class FilterData
{
    /**
     * @var \Illuminate\Contracts\Database\Eloquent\Builder
     */
    protected $builder;

    /**
     * @var array
     */
    protected $args;

    /**
     * Create a Filter Data.
     *
     * @param  \Illuminate\Database\Eloquent\Builder  $builder
     * @param  array  $args
     */
    public function __construct(Builder $builder, array $args)
    {
        $this->builder = $builder;
        $this->args = collect($args)->filter(function ($item) {
            return ! is_null($item);
        })->toArray();
    }

    /**
     * Get the builder.
     *
     * @return \Illuminate\Database\Eloquent\Builder
     */
    public function getBuilder(): Builder
    {
        return $this->builder;
    }

    /**
     * Get the args.
     *
     * @return array
     */
    public function getArgs(): array
    {
        return $this->args;
    }

    /**
     * Get single argument.
     *
     * @param  string  $key
     * @return mixed|null
     */
    public function getArgument(string $key, $default = null)
    {
        return $this->args[$key] ?? $default;
    }

    /**
     * Get single argument.
     *
     * @param  string  $key
     * @return mixed|null
     */
    public function hasArgument(string $key)
    {
        return isset($this->args[$key]);
    }
}
