<?php

namespace App\Livewire\Customer;

use App\Models\Customer;
use App\Models\User;
use App\Services\CustomerService;
use App\Traits\TableTrait;
use Filament\Forms\Components\Select;
use Filament\Forms\Components\Textarea;
use Filament\Forms\Components\TextInput;
use Filament\Forms\Concerns\InteractsWithForms;
use Filament\Forms\Contracts\HasForms;
use Filament\Tables\Actions\Action;
use Filament\Tables\Actions\CreateAction;
use Filament\Tables\Actions\DeleteAction;
use Filament\Tables\Actions\EditAction;
use Filament\Tables\Columns\TextColumn;
use Filament\Tables\Concerns\InteractsWithTable;
use Filament\Tables\Contracts\HasTable;
use Illuminate\Contracts\Pagination\Paginator;
use Illuminate\Contracts\View\View;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Foundation\Auth\Access\AuthorizesRequests;
use Livewire\Component;

class CustomerTable extends Component implements HasForms, HasTable
{
    use AuthorizesRequests, InteractsWithForms, InteractsWithTable;
    use TableTrait {
        TableTrait::table insteadof InteractsWithTable;
    }

    /**
     * Mount the component
     *
     * @return void
     */
    public function mount()
    {
        //
    }

    /**
     * Get table query.
     *
     * @return \Illuminate\Database\Eloquent\Builder
     */
    protected function getTableQuery(): Builder
    {
        return (new CustomerService)->list(asBuilder: true);
    }

    /**
     * Get table pagination Query.
     *
     * @return array
     */
    protected function paginateTableQuery(Builder $query): Paginator
    {
        /** @var LengthAwarePaginator $records */
        $records = $query->fastPaginate(
            $this->getTableRecordsPerPage(),
            ['*'],
            $this->getTablePaginationPageName(),
        );

        return $records->onEachSide(1);
    }

    /**
     * Get table columns
     *
     * @return array
     */
    protected function getTableColumns(): array
    {
        return [
            TextColumn::make('name')
                ->searchable()->sortable(),
            TextColumn::make('email')
                ->searchable()->sortable(),
            TextColumn::make('phone')
                ->searchable()->sortable(),
            TextColumn::make('assignedUser.name')
                ->searchable()->sortable(),
            TextColumn::make('created_at')
                ->dateTime(),
        ];
    }

    /**
     * Get Table Actions
     *
     * @return array
     */
    protected function getHeaderActions(): array
    {
        return [
            CreateAction::make()
                ->form([
                    TextInput::make('name')
                        ->required()
                        ->maxLength(255),
                    TextInput::make('email')
                        ->required()
                        ->maxLength(255),
                    TextInput::make('phone')
                        ->required()
                        ->maxLength(255),
                    Textarea::make('address')
                        ->required()
                        ->maxLength(255),
                ])->using(function (array $data, string $model): Customer {
                    return (new CustomerService)->create($data);
                }),
        ];
    }

    /**
     * Get Table Actions
     *
     * @return array
     */
    protected function getTableActions(): array
    {
        return [
            Action::make('assignUser')
                ->icon('heroicon-o-user')
                ->form([
                    Select::make('assigned_user_id')
                        ->label('User')
                        ->options(User::query()->pluck('name', 'id'))
                        ->required(),
                ])
                ->action(function (array $data, Customer $record): void {
                    (new CustomerService)->assignUser($record, $data);
                }),
            EditAction::make()
                ->form([
                    TextInput::make('name')
                        ->name('name')
                        ->required()
                        ->maxLength(255),
                    TextInput::make('email')
                        ->unique()
                        ->validationMessages([
                            'unique' => 'The :attribute has already been registered.',
                        ])
                        ->required()
                        ->maxLength(255),
                    TextInput::make('phone')
                        ->required()
                        ->maxLength(255),
                    Textarea::make('address')
                        ->required()
                        ->maxLength(255),
                ])->using(function (Customer $record, array $data): Customer {
                    (new CustomerService)->update($record, $data);

                    return $record;
                }),
            DeleteAction::make()
                ->using(function (Customer $record): void {
                    (new CustomerService)->delete($record);
                }),
        ];
    }

    /**
     * Records per page options
     *
     * @return array
     */
    protected function getTableRecordsPerPageSelectOptions(): array
    {
        return [10, 25, 50];
    }

    /**
     * Get the table query indentifier
     *
     * @return string
     */
    protected function getTableQueryStringIdentifier(): string
    {
        return 'customers';
    }

    /**
     * Render the role table view.
     *
     * @return \Illuminate\Contracts\View\View
     */
    public function render(): View
    {
        return view('livewire.customer.customer-table');
    }
}
