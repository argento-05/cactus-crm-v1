<?php

namespace App\Livewire\Activity;

use App\Models\Activity;
use App\Models\Customer;
use App\Models\User;
use App\Services\ActivityService;
use App\Services\CustomerService;
use App\Traits\TableTrait;
use Filament\Forms\Components\Select;
use Filament\Forms\Components\Textarea;
use Filament\Forms\Components\TextInput;
use Filament\Forms\Concerns\InteractsWithForms;
use Filament\Forms\Contracts\HasForms;
use Filament\Tables\Actions\Action;
use Filament\Tables\Actions\CreateAction;
use Filament\Tables\Actions\DeleteAction;
use Filament\Tables\Actions\EditAction;
use Filament\Tables\Actions\ViewAction;
use Filament\Tables\Columns\TextColumn;
use Filament\Tables\Concerns\InteractsWithTable;
use Filament\Tables\Contracts\HasTable;
use Filament\Tables\Filters\SelectFilter;
use Illuminate\Contracts\Pagination\Paginator;
use Illuminate\Contracts\View\View;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Foundation\Auth\Access\AuthorizesRequests;
use Livewire\Component;

class ActivityTable extends Component implements HasForms, HasTable
{
    use AuthorizesRequests, InteractsWithForms, InteractsWithTable;
    use TableTrait {
        TableTrait::table insteadof InteractsWithTable;
    }

    /**
     * Mount the component
     *
     * @return void
     */
    public function mount()
    {
        //
    }

    /**
     * Get table query.
     *
     * @return \Illuminate\Database\Eloquent\Builder
     */
    protected function getTableQuery(): Builder
    {
        return (new ActivityService)->list(asBuilder: true);
    }

    /**
     * Get table pagination Query.
     *
     * @return array
     */
    protected function paginateTableQuery(Builder $query): Paginator
    {
        /** @var LengthAwarePaginator $records */
        $records = $query->fastPaginate(
            $this->getTableRecordsPerPage(),
            ['*'],
            $this->getTablePaginationPageName(),
        );

        return $records->onEachSide(1);
    }

    /**
     * Get table columns
     *
     * @return array
     */
    protected function getTableColumns(): array
    {
        return [
            TextColumn::make('customer.name')
                ->label('Customer')
                ->searchable()->sortable(),
            TextColumn::make('customer.email')
                ->label('Email')
                ->searchable()->sortable(),
            TextColumn::make('customer.phone')
                ->label('Phone')
                ->searchable()->sortable(),
            TextColumn::make('user.name')
                ->searchable()->sortable(),
            TextColumn::make('description')->limit(25),
            TextColumn::make('type'),
            TextColumn::make('status')
                ->badge()
                ->color(fn (string $state): string => match ($state) {
                    'open' => 'warning',
                    'completed' => 'success',
                    'canceled' => 'danger',
            }),
            TextColumn::make('created_at')
                ->dateTime(),
        ];
    }

    /**
     * Get Table Actions
     *
     * @return array
     */
    protected function getHeaderActions(): array
    {
        return [
            CreateAction::make()
                ->form([
                    Select::make('customer_id')
                        ->label('Customer')
                        ->options(auth()->user()->getMyCustomers()->pluck('name', 'id'))
                        ->required(),
                    Select::make('type') //TODO: use PHP enums
                        ->options([
                            'call' => 'Call',
                            'email' => 'Email',
                            'meeting' => 'Meeting',
                        ]),
                    Textarea::make('description')->rows(10)
                ])->using(function (array $data, string $model): Activity {
                    return (new ActivityService)->create($data);
                }),
        ];
    }

    /**
     * Get Table Actions
     *
     * @return array
     */
    protected function getTableActions(): array
    {
        return [
            EditAction::make()
                ->form([
                    Select::make('customer_id')
                        ->label('Customer')
                        ->options(auth()->user()->getMyCustomers()->pluck('name', 'id'))
                        ->required(),
                    Select::make('type') //TODO: use PHP enums
                        ->options([
                            'call' => 'Call',
                            'email' => 'Email',
                            'meeting' => 'Meeting',
                        ]),
                    Select::make('status') //TODO: use PHP enums
                        ->options([
                            'open' => 'Open',
                            'completed' => 'Completed',
                            'canceled' => 'Canceled',
                        ]),
                    Textarea::make('description')->rows(10)

                ])->using(function (Activity $record, array $data): Activity {
                    (new ActivityService)->update($record, $data);

                    return $record;
                })->visible(fn (Activity $record): bool => $record->status == 'open'),
            DeleteAction::make()
                ->using(function (Activity $record): void {
                    (new ActivityService)->delete($record);
                })->visible(fn (Activity $record): bool => $record->status == 'open'),
            ViewAction::make()
                ->form([
                    Select::make('customer_id')
                        ->label('Customer')
                        ->options(auth()->user()->getMyCustomers()->pluck('name', 'id'))
                        ->required(),
                    Select::make('type') //TODO: use PHP enums
                        ->options([
                            'call' => 'Call',
                            'email' => 'Email',
                            'meeting' => 'Meeting',
                        ]),
                    Select::make('status') //TODO: use PHP enums
                        ->options([
                            'open' => 'Open',
                            'completed' => 'Completed',
                            'canceled' => 'Canceled',
                        ]),
                    Textarea::make('description')->rows(10)
                ])
        ];
    }

    /**
     * Get table filters
     */
    protected function getTableFilters(): array
    {
        return [
            SelectFilter::make('status')
                ->label('Status')
                ->options(
                    function () {
                        return [
                            'Open' => 'Open',
                            'Completed' => 'Completed',
                            'Canceled' => 'Canceled',
                        ];
                    }
                )
                ->query(function (Builder $query, array $data) {
                    if (! empty($data['value'])) {
                        $query->where('status', $data['value']);
                    }
            }),
        ];
    }

    /**
     * Records per page options
     *
     * @return array
     */
    protected function getTableRecordsPerPageSelectOptions(): array
    {
        return [10, 25, 50];
    }

    /**
     * Get the table query indentifier
     *
     * @return string
     */
    protected function getTableQueryStringIdentifier(): string
    {
        return 'customers';
    }

    /**
     * Render the role table view.
     *
     * @return \Illuminate\Contracts\View\View
     */
    public function render(): View
    {
        return view('livewire.customer.customer-table');
    }
}
