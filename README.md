# Cactus CRM v1

This is a Laravel project template with all the necessary configurations to start building a web application using the Laravel framework.

## Requisites

PHP 8.2, NodeJs v16.0.0

## Installation

Please check the official laravel installation guide for server requirements before you start. [Official Documentation](https://laravel.com/docs/10.x/installation#installation)

Clone the repository

    git clone https://gitlab.com/argento-05/cactus-crm-v1

Switch to the repo folder

    cd cactus-crm-v1

Install all the dependencies using composer

    composer install

Copy the example env file and make the required configuration changes in the .env file

    cp .env.example .env

Generate a new application key

    php artisan key:generate

Run the database migrations (**Set the database connection in .env before migrating**)

    php artisan migrate --seed

Install assets
    
    yarn

Compile assets
    
    yarn run build

Start the local development server

    php artisan serve

You can now access the server at http://localhost:8000

**TL;DR command list**

    git clone [Repository url]
    cd [Repository folder]
    composer install
    cp .env.example .env
    php artisan key:generate
    php artisan migrate --seed
    yarn
    yarn run build
    composer check # to fix sintax
    
**Make sure you set the correct database connection information before running the migrations** [Environment variables](#environment-variables)

    php artisan migrate
    php artisan serve


***Note*** : It's recommended to have a clean database before seeding. You can refresh your migrations at any point to clean the database by running the following command

    php artisan migrate:refresh


## Testing

Users (password: 123456):
- admin@cactustech.co
- operator@cactustech.co
----------

# Code overview

## Environment variables

- `.env` - Environment variables can be set in this file

***Note*** : You can quickly set the database information and other variables in this file and have the application fully working.

----------

