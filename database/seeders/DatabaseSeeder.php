<?php

namespace Database\Seeders;

// use Illuminate\Database\Console\Seeds\WithoutModelEvents;

use App\Models\Customer;
use App\Models\User;
use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     */
    public function run(): void
    {

        // \App\Models\User::factory(10)->create();

        //Admin user
        \App\Models\User::factory()->create([
            'name' => 'Admin Cactus',
            'email' => 'admin@cactustech.co',
            'password' => bcrypt('123456'),
        ]);

        //Operator user
        \App\Models\User::factory()->create([
            'name' => 'Operator Cactus',
            'email' => 'operator@cactustech.co',
            'password' => bcrypt('123456'),
        ]);

        //Customers
        for ($i = 0; $i < 50; $i++) {
            \App\Models\Customer::factory()->create();
        }

        //Asign users to customers
        $customers = Customer::all();
        foreach ($customers as $customer) {
            $user = User::inRandomOrder()->first();
            $customer->assigned_user_id = $user->id;
            $customer->save();
        }

        //Activities
        for ($i = 0; $i < 100; $i++) {
            \App\Models\Activity::factory()->create();
        }
    }
}
