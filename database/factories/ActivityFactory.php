<?php

namespace Database\Factories;

use App\Models\User;
use Illuminate\Database\Eloquent\Factories\Factory;

/**
 * @extends \Illuminate\Database\Eloquent\Factories\Factory<\App\Models\Activity>
 */
class ActivityFactory extends Factory
{
    /**
     * Define the model's default state.
     *
     * @return array<string, mixed>
     */
    public function definition(): array
    {
        $user = User::all()->random();
        $customer = $user->getMyCustomers()->random()->id;

        return [
            'customer_id' => $customer,
            'user_id' => $user->id,
            'description' => fake()->text(),
            'type' => fake()->randomElement(['call', 'email', 'meeting']),
            'status' => fake()->randomElement(['open', 'completed', 'canceled']),
        ];
    }
}
